#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include <iostream>
#include <string>

#define LISTEN_PORT 458

void SendString (TCPsocket conn, std::string str)
{
	SDLNet_TCP_Send(conn,str.data(),str.size());
};

#define SDL_main main
int main ()
{
	if (SDL_Init(0) < 0)
		throw std::runtime_error("Failed to initialize SDL");
	if (SDLNet_Init())
		throw std::runtime_error("Failed to initialize SDL_net");

	IPaddress addr;
	SDLNet_ResolveHost(&addr,"localhost",LISTEN_PORT);
	TCPsocket conn = SDLNet_TCP_Open(&addr);
	if (!conn)
		throw std::runtime_error("Failed to connect to database server");

	SendString(conn,"test text.");


	getchar();
	return 0;
};
