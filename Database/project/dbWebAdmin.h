#ifndef DBWEBADMIN_H_INCLUDED
#define DBWEBADMIN_H_INCLUDED

#include <SDL2/SDL.h>
#include <smh/smh.h>

/// There should be no more than one instance of dbWebAdmin!

class dbWebAdmin
{
	static void HandleRequest (Client* client);
	void AdminMain ();
	static void ExecAdminMain (dbWebAdmin* admin);
	static bool sig_exit;
	SDL_Thread* watcher_thread;

	public:
	dbWebAdmin (Uint16 port);
	bool CheckExit (); // If it returns true, the admin server has been shut down.
};

#endif // DBWEBADMIN_H_INCLUDED
