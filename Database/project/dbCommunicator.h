#ifndef DBCOMMUNICATOR_H_INCLUDED
#define DBCOMMUNICATOR_H_INCLUDED

#include "dbConnection.h"
#include <iostream>

class dbCommunicator
{
	private:
	dbConnection connection;
	void Communicate ();
	static void ExecCommunicate (dbCommunicator* communicator);

	public:
	dbCommunicator (dbConnection conn);
};

#endif // DBCOMMUNICATOR_H_INCLUDED
