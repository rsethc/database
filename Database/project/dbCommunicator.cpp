#include "dbCommunicator.h"

static void dbCommunicator::ExecCommunicate (dbCommunicator* communicator)
{
	communicator->Communicate();
	delete communicator;
};

dbCommunicator::dbCommunicator (dbConnection conn) : connection(conn)
{
	SDL_DetachThread(SDL_CreateThread(ExecCommunicate,"Communication",this));
};

void dbCommunicator::Communicate ()
{
	std::cout << "communicating with a socket" << '\n';
};
