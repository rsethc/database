#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include "dbListener.h"
#include "dbWebAdmin.h"
#include <iostream>

#define LISTEN_PORT 458
#define ADMIN_PORT 430

#define SDL_main main
int main ()
{
	if (SDL_Init(0) < 0)
		throw std::runtime_error("Failed to initialize SDL");
	if (SDLNet_Init())
		throw std::runtime_error("Failed to initialize SDL_net");

	dbWebAdmin admin(ADMIN_PORT);

	dbListener listener(LISTEN_PORT);

	while (!admin.CheckExit()) SDL_Delay(1000);

	return 0;
};
