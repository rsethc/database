#ifndef DBCONNECTION_H_INCLUDED
#define DBCONNECTION_H_INCLUDED

#include <SDL2/SDL_net.h>

class dbConnection
{
	private:
	TCPsocket socket;

	public:
	dbConnection (TCPsocket socket);
	~dbConnection ();
};

#endif // DBCONNECTION_H_INCLUDED
