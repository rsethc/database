#include "dbWebAdmin.h"

#include <smh/smh.h>
#include <mime.h>
#include <iostream>
#include <ctime>

// SMH server stats
extern SDL_atomic_t ListenersActive;
extern volatile time_t InitializedAtTime;
extern Uint64 RequestsEverServed;
extern Uint64 RequestsServedLastSecond;
extern SDL_atomic_t OpenConnections;

char* alloc_strcat (char* str1, char* str2)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	char* out = malloc(len1 + len2 + 1);
	memcpy(out,str1,len1);
	memcpy(out + len1,str2,len2);
	out[len1 + len2] = 0;
	return out;
};

bool dbWebAdmin::sig_exit = false;

static void dbWebAdmin::HandleRequest (Client* client)
{
	std::cout << "processing admin request\n";

	FILE* file;
	if (!strcmp(client->path,"admin/shutdown"))
	{
		sig_exit = true;
		Status200(client,NULL);
		SendEmpty(client);
	}
	else if (!strcmp(client->path,"admin/stats"))
	{
		time_t timenow = time(NULL);
		int seconds = timenow - InitializedAtTime;
		int minutes = seconds / 60;
		int hours = minutes / 60;
		int days = hours / 24;
		seconds -= minutes * 60;
		minutes -= hours * 60;
		hours -= days * 24;

		char* uptime_str;
		//asprintf(&str,"%u",SDL_GetTicks() / 1000);
		asprintf(&uptime_str,"%d days, %d hours, %d minutes, %d seconds",days,hours,minutes,seconds);



		char* requests_ever_str;
		asprintf(&requests_ever_str,"%llu",RequestsEverServed);



		char* active_conns_str;
		asprintf(&active_conns_str,"%u",SDL_AtomicGet(&OpenConnections));



		char* requests_last_second_str;
		asprintf(&requests_last_second_str,"%llu",RequestsServedLastSecond);



		char* full_response_str;
		asprintf(&full_response_str,"uptime=%s;served=%s;active-connections=%s;served-last-second=%s",uptime_str,requests_ever_str,active_conns_str,requests_last_second_str);



		Status200(client,"text/plain");
		HTTP_SendStringContent(client,full_response_str);

		free(full_response_str);
		free(uptime_str);
		free(requests_ever_str);
		free(active_conns_str);
		free(requests_last_second_str);
	}
	else
	{
		//std::cout << client->path << " -> ";
		if (!*client->path) client->path = "index.html";
		//std::cout << client->path << " -> ";

		char* fullpath = alloc_strcat("AdminPages/",client->path);
		//std::cout << fullpath << "\n";
		file = fopen(fullpath,"rb");
		free(fullpath);
		if (file)
		{
			Status200(client,DeduceMimeType(client->path));
			HTTP_SendFile(client,file);
		}
		else
		{
			Error404(client);
		};
	};
	//std::cout << "done with request\n";
};

void dbWebAdmin::AdminMain ()
{
	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive))
			throw std::runtime_error("Failed to establish admin listener");
		if (sig_exit)
			break;
		SMH_StatsStep();
	};

	std::cout << "Received stop signal from web admin\n";

	HTTP_Quit();
	sig_exit = true;
};

void dbWebAdmin::ExecAdminMain (dbWebAdmin* admin) { admin->AdminMain(); };

dbWebAdmin::dbWebAdmin (Uint16 port)
{
	if (HTTP_Init())
		throw std::runtime_error("Failed to initialize SMH server");

	StartListening(port,ServeClient_HTTP,HandleRequest);

	watcher_thread = SDL_CreateThread(ExecAdminMain,"Web Admin Watcher",this);
};

bool dbWebAdmin::CheckExit ()
{
	bool signal = sig_exit;
	if (signal) SDL_WaitThread(watcher_thread,NULL);
	return signal; // Do NOT directly return sig_exit since it could have changed after being used for the 'if'.
};
