#include "dbListener.h"

#include <iostream>
#include "dbConnection.h"
#include "dbCommunicator.h"

void dbListener::Listen ()
{
	std::cout << "beginning to listen\n";
	while (!stop_sem)
	{
		if (SDLNet_CheckSockets(socket_set,1000))
		{
			std::cout << "client is connecting\n";
			TCPsocket client_socket = SDLNet_TCP_Accept(listen_socket);
			dbConnection connection(client_socket);
			dbCommunicator* communicator = new dbCommunicator(connection); // Fire-and-forget
		};
	};
	std::cout << "closing listen thread\n";
	SDL_SemPost(stop_sem);
};

static void dbListener::ExecListen (dbListener* listener) { listener->Listen(); };

dbListener::dbListener (Uint16 port)
{
	//std::cout << "constructor called\n";
	IPaddress ip;
	SDLNet_ResolveHost(&ip,NULL,port);
	//SDLNet_ResolveHost(&ip,"localhost",80);
	listen_socket = SDLNet_TCP_Open(&ip);
	if (!listen_socket)
	{
		std::string errtext = "Can't listen on port ";
		errtext += std::to_string(port);
		throw std::runtime_error(errtext);
	};
	//std::cout << "listen success\n";
	std::cout << "Listening on port " << port << '\n';
	socket_set = SDLNet_AllocSocketSet(1);
	SDLNet_TCP_AddSocket(socket_set,listen_socket);
	SDL_DetachThread(SDL_CreateThread(ExecListen,"Listening",this));
};

dbListener::~dbListener ()
{
	StopListening(); // Ensure the thread is shut down before cleaning up.
	SDL_DestroySemaphore(stop_sem);
	//std::cout << "destructor called\n";
	SDLNet_FreeSocketSet(socket_set);
	SDLNet_TCP_Close(listen_socket);
};

void dbListener::StopListening ()
{
	if (!stop_sem) // Do not try to shut down the thread twice.
	{
		std::cout << "waiting for thread to stop\n";
		stop_sem = SDL_CreateSemaphore(0);
		SDL_SemWait(stop_sem);
		std::cout << "thread has stopped\n";
	};
};

